// const axios = require('axios')
// const url = 'http://checkip.amazonaws.com/';
let response;

/**
 *
 * Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format
 * @param {Object} event - API Gateway Lambda Proxy Input Format
 *
 * Context doc: https://docs.aws.amazon.com/lambda/latest/dg/nodejs-prog-model-context.html 
 * @param {Object} context
 *
 * Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
 * @returns {Object} object - API Gateway Lambda Proxy Output Format
 * 
 */
 const https = require('https');
 var AWS = require("aws-sdk");
 var SNS = new AWS.SNS();

exports.lambdaHandler = async (event, context) => {
    const message = "wezwał Cię i potrzebuje Twojej pomocy!";
    const topic = "arn:aws:sns:eu-west-1:230277585050:notify-hubert";
    await publishSNS(message, topic);
    try {
        // const ret = await axios(url);
        response = {
            'statusCode': 200,
            'body': JSON.stringify({
                message: 'Hubert dostał SMSa',
                // location: ret.data.trim()
            })
        }
    } catch (err) {
        console.log(err);
        return err;
    }
    return response
};


async function publishSNS(message, topicArn) {
    await SNS.publish({
        Message: message,
        TargetArn: topicArn
    }).promise().then((data) => {
        console.log('SNS push succeeded: ', data);
    }).catch((err) => {
        console.error(err);
    });
}